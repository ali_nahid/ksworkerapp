package com.ks.workerapp;

//import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class KSWorkerApplication {

	public static void main(String[] args) {
		//SpringApplication.run(KSWorkerApplication.class, args);
		new SpringApplicationBuilder(KSWorkerApplication.class).web(WebApplicationType.NONE).run();
	}

	
}
