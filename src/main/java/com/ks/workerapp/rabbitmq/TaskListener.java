package com.ks.workerapp.rabbitmq;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ks.workerapp.pojos.BigTask;
import com.ks.workerapp.pojos.SampleResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class TaskListener {
    
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${sample.mock.url}")
    private String mockUrl;

    @Autowired
    private RestTemplate restTemplate;
    
    public void receiveBigTaskMessage(String message) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            BigTask bigTask = mapper.readValue(message, BigTask.class);
            int tasksCount = bigTask.getjData().size();
            String opsSize = "BIG";
            int delaySec = 20000;
            if(tasksCount < 10) {
                opsSize = "TINY";
                delaySec = 5000;
            } else if(tasksCount < 80) {
                opsSize = "SMALL";
                delaySec = 10000;
            }
            String url = mockUrl.replace("[DELAY_SEC]", delaySec+"");
            this.logger.info("payload received "+opsSize+": " + bigTask.toString()+ " making big IO call to: " +url);
            SampleResponse response = this.restTemplate.getForObject(url, SampleResponse.class);
            this.logger.info("Big IO operation done. Response: " + response.toString());
        } catch(JsonProcessingException jpe) {

        }
        
    }
}