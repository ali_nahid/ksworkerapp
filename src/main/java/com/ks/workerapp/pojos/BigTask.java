package com.ks.workerapp.pojos;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BigTask  {

    /**
     *
     */
    //private static final long serialVersionUID = 4695715020926478523L;

    @JsonProperty("taskId")
    private String taskId;

    @JsonProperty("jdata")
    private List<JData> jData;

    /*public BigTask() {
        this.taskId = UUID.randomUUID().toString();
    }*/

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskId() {
        return this.taskId;
    }

    public List<JData> getjData() {
        return jData;
    }

    public void setjData(List<JData> jData) {
        this.jData = jData;
    }

    @Override
    public String toString() {
        String s= "taskId: "+ this.taskId;
        if(this.jData != null && !this.jData.isEmpty()) {
            s += "jdata last: "+this.jData.get(this.jData.size()-1).getEmail();
        }
        return s;
    }


}